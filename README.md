In this project I will implement a facial recognition alogorithm that detects and classifies each indivisual face into it's respictive owner.

This project involves multiple data preprocessing techniques as well as building a convulutional neural net model capable of classifying faces dynamically and in an accurate manner. It utilizes effectively many deep learning libraries such as tensorflow, as well as uses some machine learning techniques to preprocess data. Additionaly, all preprocessing phases such as labeling of pictures and image resizing is done from scratch inside this project.

Before starting the script please make sure to download the following modules:

Tensorflow (v.2)
Pandas
Numpy
MTCNN
MatplotLib

As well as the following dataset:

LFW 

Please make sure to change the destination data while reading the datasets from pandas for the scipt to actually work.
All data rights reserved to their original owners.
