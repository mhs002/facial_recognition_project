import numpy as np

data = np.load("Celebrity_Faces.npz",allow_pickle=True)

train_x,train_y= data['arr_0'],data['arr_1']

test_x,test_y,train_data= [],[],[]
count = 0
 
# Extract the testing data and fix the training data
# import ipdb;ipdb.set_trace()
for pic in train_x:
    picture = pic[0].astype('float32')
    test_x.append(pic[0])
    test_y.append(train_y[count][0])
    train_x[count].pop(0)
    train_y[count].pop(0)
    count += 1
    print(count)

count = 0
for pic in train_x:
    for picture in pic:
        picture_pix = picture.astype('float32')
        train_data.append(picture_pix)

train_data = np.array(train_data)

test_x,test_y = np.array(test_x),np.array(test_y)

np.savez_compressed("Celebrity_Faces_Train_",train_data,train_y)
np.savez_compressed("Celebrity_Faces_Test_",test_x,test_y)
