import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers,models
import matplotlib.pyplot as plt 

data_train = np.load("Celebrity_Faces_Train_.npz",allow_pickle=True)
data_test = np.load("Celebrity_Faces_Test_.npz",allow_pickle=True)

train_x,train_y,test_x,test_y = data_train['arr_0'],data_train['arr_1'],data_train['arr_0'],data_train['arr_1']

class_names = ['Jan Ullrich','Ai Sugiyama','Elena Bovina','Luis Ernesto Derbez Bautista','Laura Linney','Mack Brown','Carlos Bianchi','Damon Van Dam','Dianne Feinstein','John Stockton','Robert Mueller','George Roy Hill','Patricia Heaton','Xavier Malisse','Edmund Stoiber','Kenneth Bowersox','Aicha El Ouafi','Junichiro Koizumi']

train_y = [x for x in train_y if x != []]
t = []
count = 0

# import ipdb;ipdb.set_trace()

for i in train_y:
    for im in i:
        t.append([count])
    count +=1 

tr = []
test_image = []
tr_l = []


for x in range(80):
    tr.append(train_x[x])

train_label = np.array(t)

tr_ = []
for x in range(80):
    tr_.append(train_label[x])

for x in [0,54]:
    test_image.append(tr[x])
    tr.pop(x)

tr_1 = np.array(tr)
test_image = np.array(test_image)

for x in [0,17]:
    tr_l.append(np.array([x]))
    tr_.remove(x)

tr_l = np.array(tr_l)
tr_label = np.array(tr_)

tr_1,test_image = tr_1 / 255.0, test_image / 255.0

model = keras.models.Sequential([
            keras.layers.Conv2D(32,kernel_size=(3,3),activation="relu",padding="valid"),
            keras.layers.MaxPooling2D((2,2)),

            keras.layers.Conv2D(64,kernel_size=(3,3),activation="relu",padding="valid"),
            keras.layers.MaxPooling2D((2,2)),

            keras.layers.Conv2D(64,kernel_size=(3,3),activation="relu",padding="valid"),
            keras.layers.MaxPooling2D((2,2)),

            keras.layers.Flatten(input_shape=(160,160,3)),
            keras.layers.Dense(128,activation="relu"),
            keras.layers.Dense(18,activation="softmax")

        ])

model.compile(
        optimizer = "adam",
        loss = "sparse_categorical_crossentropy",
        metrics = ["accuracy"]
        )

model.fit(tr_1,tr_label,epochs=12,batch_size=1)
# model.summary()

test_loss , test_acc = model.evaluate(test_image,tr_l,verbose=1)

print("The test accuracy is: {}".format(test_acc))

predictions = model.predict(test_image)
print(class_names[np.argmax(predictions[1])])

plt.figure()
plt.imshow(test_image[1])
plt.colorbar()
plt.grid(False)
plt.show()

