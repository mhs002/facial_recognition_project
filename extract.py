from PIL import Image
import numpy as np
from mtcnn.mtcnn import MTCNN
from matplotlib import pyplot as plt
import os

def extract_face(image,size=(160,160)):
    image = Image.open(image)
    image.thumbnail((200,200))
    pix_image = np.asarray(image)
    det = MTCNN()
    results = det.detect_faces(pix_image)
    x,y,w,h = results[0]['box']
    x,y = abs(x),abs(y)
    x2,y2 = x+w , y+h # Resolves the box
    face = pix_image[y:y2,x:x2]
    image = Image.fromarray(face)
    image = image.resize(size)
    face_ = np.asarray(image)
    return face_ 

def load_faces(directory):
    count = 0
    faces_train = []
    faces_test = []
    for img in os.listdir(directory):
        print(directory)
        if count < os.path.getsize(directory):
            face = extract_face(directory+"/"+img)
            faces_train.append(face)
            count+=1
        else:
            faces_test.append(face)
    return faces_train,faces_test

def load_datasets(directory):
    X,Y,X_test= [],[],[]
    count=0
    for subdir in os.listdir(directory):
        count+=1
        path = directory + "/" + subdir 
        faces = load_faces(path)[0]
        face_test = load_faces(path)[1]
        label = [subdir for _ in faces]
        # print("Loaded {} for the class {}".format(len(faces),subdir))
        print(count)
        X.append(faces)
        X_test.append(face_test)
        Y.append(label)
    return np.asarray(X),np.asarray(Y),np.asarray(X_test)

train_x,train_y,test_x = load_datasets("/home/mohammad/Desktop/machine_learning/facial_rec_project/lfw")
test_y = train_y

np.savez_compressed("Celebrity Faces",train_x,train_y,test_x,test_y)
